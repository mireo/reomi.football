<?php
namespace Reomi\Football\DataSource;

use Neos\Neos\Service\DataSource\AbstractDataSource;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Reomi\Football\Service\ApiService;

class CompetitionsSource extends AbstractDataSource {

    /**
     * @var string
     */
    static protected $identifier = 'select-competitions-source';

    /**
     * Get data
     *
     * @param NodeInterface $node The node that is currently edited (optional)
     * @param array $arguments Additional arguments (key / value)
     * @return array JSON serializable data
     */
    public function getData(NodeInterface $node = NULL, array $arguments)
    {
        $arrayOfelements = array();

        $request = new ApiService(array('timeFrame' => 'n10'));
        $request->getCompetitions();
        $response = $request->getResponse();
        $responseData = json_decode($response, 1);

        $request->addFilter('season', (new \DateTime())->format('Y'));
        $request->getCompetitions();
        $response2 = $request->getResponse();
        $responseData2 = json_decode($response2,1);

        $responseData = array_merge($responseData, $responseData2);

        if($responseData) foreach($responseData as $fetchedItem ) {
            $value = $fetchedItem['id'];
            $label = $fetchedItem['caption'].' ('.$fetchedItem['year'].')';

            $tempArray = array (
                'value' => $value,
                'label' => $label
            );
            array_push($arrayOfelements, $tempArray);
        }


        return $arrayOfelements;
    }

}