<?php
namespace Reomi\Football\Service;

use Neos\Flow\Exception;
use Neos\Flow\Http\Client\Browser;
use Neos\Flow\Http\Client\CurlEngine;
use Neos\Flow\Http\Request;
use Neos\Flow\Http\Uri;

class ApiService{
    /**
     * @var string
     */
    private $token = '573d0119fcfd44dfa029a8662c243421';

    /**
     * @var mixed
     */
    private $response;

    /**
     * @var string
     */
    private $uri = 'http://api.football-data.org/v1/';

    /**
     * @var array
     */
    private $filters;

    function __construct($filters = null){
        if( $filters )
            $this->filters = $filters;
    }

    /**
     * @param $url
     * @return string
     */
    private function appendFilters($url){
        if( !$this->filters )
            return $url;
        return $url.'?'.http_build_query($this->filters);
    }

    /**
     * @param mixed $key
     * @param string $value
     */
    public function addFilter($key = null, $value = null){
        if( empty($key) )
            return;
        if( is_array($key) ){
            $this->filters = array_merge($this->filters, $key);
        }elseif( !empty($value) ) {
            $this->filters[$key] = $value;
        }
    }

    /**
     * @param mixed $key
     */
    public function removeFilter($key = null){
        if( empty($key) )
            $this->filters = array();
        elseif( isset($this->filters[$key]) )
            unset($this->filters[$key]);
    }

    public function getCompetitions(){
        $test = '464/fixtures?timeFrame=n10';
        $url = $this->uri.'competitions';
        $url = $this->appendFilters($url);
        $this->response = $this->sendCustomRequest($url);
    }

    public function getCompetition($id){
        $url = $this->uri.'competitions/'.$id;
        $url = $this->appendFilters($url);
        $this->response = $this->sendCustomRequest($url);
    }

    public function getMatchesForCompetition($competition){
        $url = $this->uri.'competitions/'.$competition.'/fixtures';
        $url = $this->appendFilters($url);
        $this->response = $this->sendCustomRequest($url);
    }

    public function getMatch($id){
        $url = $this->uri.'fixtures/'.$id;
        $url = $this->appendFilters($url);
        $this->response = $this->sendCustomRequest($url);
    }

    public function getTeam($id){
        $url = $this->uri.'teams/'.$id;
        $url = $this->appendFilters($url);
        $this->response = $this->sendCustomRequest($url);
    }

    public function sendCustomRequest($url){
        $browser = new Browser();
        $browser->setRequestEngine(new CurlEngine());

        $uriObj = new Uri( $url );

        $request = Request::create($uriObj, 'GET');

        if( $this->token )
            $request->setHeader('X-Auth-Token', $this->token);

        return $browser->sendRequest($request);
//        \Neos\Flow\var_dump($this->response->getContent());die();
    }

    public function getResponse(){
        return $this->response;
    }
}