<?php
namespace Reomi\Football\Service;

use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\Eel\FlowQuery\FlowQuery;
use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages as Error;
use Neos\ContentRepository\Domain\Service\ContextFactoryInterface;
use Neos\ContentRepository\Domain\Service\NodeTypeManager;
use Neos\Flow\Exception;
use Neos\Flow\Mvc\FlashMessageContainer;
use Neos\Flow\Security\Context;
use Neos\ContentRepository\Domain\Model\NodeTemplate;
use Neos\ContentRepository\Utility as CrUtitlity;

/**
 * Class BetService
 * @package Reomi\Football\Service
 * @Flow\Scope("singleton")
 */
class BetService {

    /**
     * @var string
     * @Flow\InjectConfiguration(path="contentRepository.rootNodeName")
     */
    protected $rootNodeName;
    /**
     * @var string
     * @Flow\InjectConfiguration(path="contentRepository.rootNodeType")
     */
    protected $rootNodeType;
    /**
     * @var string
     * @Flow\InjectConfiguration(path="contentRepository.betNodeType")
     */
    protected $betNodeType;

    /**
     * @Flow\Inject
     * @var NodeTypeManager
     */
    protected $nodeTypeManager;

    /**
     * @Flow\Inject
     * @var ContextFactoryInterface
     */
    protected $contextFactory;

    /**
     * @var Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @var NodeInterface
     */
    protected $rootNode;

    /**
     * @Flow\Inject
     * @var FlashMessageContainer
     */
    protected $flashMessageContainer;

    protected function getRootNode(){
        if( !$this->rootNode ) {
            $context = $this->contextFactory->create(array(
                'workspaceName' => 'live'
            ));
            $rootNode = $context->getRootNode()->getNode('sites');
            $this->rootNode = $rootNode->getNode($this->rootNodeName);
            if( !$this->rootNode ) {
                $path = explode('/', $this->rootNodeName);
                if ($path) foreach ($path as $p) {
                    $node = $rootNode->getNode($p);
                    if (!$node) {
                        $rootNode = $this->createRootNode($p, $rootNode);
                    }else{
                        $rootNode = $node;
                    }
                }
                $this->rootNode = $rootNode;
            }
        }
        return $this->rootNode;
    }


    /**
     * @param $identifier
     * @param array $properties
     * @param array $registerBetProperties
     * @return \Neos\ContentRepository\Domain\Model\NodeInterface
     */
    public function createBet($identifier, $properties = array(), $registerBetProperties = array()) {

//        if(!$identifier)
//            throw new Exception('Identifier is ')

        $node = $this->checkIfRegisterBetExists($identifier);

        if( !$node ){
            $node = $this->createRegisterBet($identifier, $registerBetProperties);
        }

        if( $this->checkIfBetExists($identifier) ){
//            \Neos\Flow\var_dump($node);die('weszlo');
            $this->flashMessageContainer->addMessage(
                new Error\Error('You already bet this game')
            );
            return false;
        }

//        \Neos\Flow\var_dump($node);die();

        $rootCollection = $node->getNode('bets');

        $nodeType = $this->nodeTypeManager->getNodeType($this->betNodeType);
        $nodeTemplate = new NodeTemplate();

        $title = 'Bet';
        $nodeTemplate->setNodeType($nodeType);

        if( $properties && is_array($properties) ) foreach($properties as $key=>$value)
            $nodeTemplate->setProperty($key, $value);

        $account = $this->securityContext->getAccountByAuthenticationProviderName('Hb180.FrontendLogin:Frontend');
        $nodeTemplate->setName($account->getAccountIdentifier());

        return $rootCollection->createNodeFromTemplate($nodeTemplate);
    }

    /**
     * @param $identifier
     * @param array $properties
     * @return \Neos\ContentRepository\Domain\Model\NodeInterface
     */
    public function createRegisterBet($identifier, $properties = array()){
        $rootNode = $this->getRootNode();

        $nodeType = $this->nodeTypeManager->getNodeType($this->rootNodeType);
        $nodeTemplate = new NodeTemplate();
        $nodeTemplate->setNodeType($nodeType);

//        $title = 'Game';

        $nodeTemplate->setName(CrUtitlity::renderValidNodename($identifier));

        if( $properties && is_array($properties) ) foreach($properties as $key=>$value)
            $nodeTemplate->setProperty($key, $value);
//        $nodeTemplate->setIdentifier($identifier);
//        $nodeTemplate->setProperty('title',$title);

        return $rootNode->createNodeFromTemplate($nodeTemplate);

    }

    /**
     * @param $identifier
     * @return mixed
     */
    public function checkIfRegisterBetExists($identifier){
        $rootNode = $this->getRootNode();
        return $rootNode->getNode($identifier)??FALSE;
    }

    public function checkIfBetExists($identifier){
        $rootNode = $this->getRootNode();
        $node = $this->checkIfRegisterBetExists($identifier);

        if(!$node)
            return FALSE;

//        \Neos\Flow\var_dump($node);die();

        $account = $this->securityContext->getAccountByAuthenticationProviderName('Hb180.FrontendLogin:Frontend');
        $accountID = $account->getAccountIdentifier();

        return $node->getNode('bets')->getNode($accountID)??FALSE;
    }

    /**
     * @param $identifier
     * @param \Neos\ContentRepository\Domain\Model\NodeInterface $context
     * @return \Neos\ContentRepository\Domain\Model\NodeInterface
     */
    public function createRootNode($identifier, $context){

        $nodeType = $this->nodeTypeManager->getNodeType('Neos.NodeTypes:Page');
        $nodeTemplate = new NodeTemplate();
        $nodeTemplate->setNodeType($nodeType);

        $nodeTemplate->setName(CrUtitlity::renderValidNodename($identifier));

//        if( $properties && is_array($properties) ) foreach($properties as $key=>$value)
//            $nodeTemplate->setProperty($key, $value);

        return $context->createNodeFromTemplate($nodeTemplate);

    }
}