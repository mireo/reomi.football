<?php
namespace Reomi\Football\FusionObjects;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\ActionRequest;
use Neos\Fusion\FusionObjects\TemplateImplementation as OriginalTemplateImplementation;
use Neos\Fusion\FusionObjects\Helpers as Helpers;
use Neos\FluidAdaptor\Core\Parser\Interceptor\ResourceInterceptor;
use TYPO3Fluid\Fluid\Core\Parser\InterceptorInterface;
use Neos\Flow\Http\Client\Browser;
use Neos\Flow\Http\Client\CurlEngine;
use Neos\Flow\Http\Request;
use Neos\Flow\Http\Uri;
use Reomi\Football\Service\ApiService;

class SheduledMatchesImplementation extends TemplateImplementation{

    /**
     * This is a template method which can be overridden in subclasses to add new variables which should
     * be available inside the Fluid template. It is needed e.g. for Expose.
     *
     * @param Helpers\FluidView $view
     * @return void
     */
    protected function initializeView(Helpers\FluidView $view)
    {

        $request = new ApiService(array('timeFrame' => 'n10'));
        $request->getMatchesForCompetition($this->offsetGet('competition'));
        $response = $request->getResponse();

        if( !$response )
            return;

        $data = json_decode($response->getContent(), true);
        if( $data && isset($data['fixtures']) ) foreach( $data['fixtures'] as $key=>$value ){
            if( $value['_links'] ) foreach( $value['_links'] as $k=>$link ){
                $exploded = explode('/',$value['_links'][$k]['href']);
                $data['fixtures'][$key]['data'][$k] = $exploded[ count($exploded)-1 ];
            }


//            $exploded = explode('/',$value['_links']['awayTeam']['href']);
//            $data['fixtures'][$key]['data']['awayTeamId'] = $exploded[ count($exploded)-1 ];
//
//            $exploded = explode('/',$value['_links']['homeTeam']['href']);
//            $data['fixtures'][$key]['data']['homeTeamId'] = $exploded[ count($exploded)-1 ];
//
//            $exploded = explode('/',$value['_links']['self']['href']);
//            $data['fixtures'][$key]['data']['matchId'] = $exploded[ count($exploded)-1 ];
            $data['fixtures'][$key]['serialized'] = base64_encode(serialize($data['fixtures'][$key]));
        }


        $view->assign('responseData', $data);
        // template method
    }
}