<?php
namespace Reomi\Football\Controller;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\I18n\Locale;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Security\Context;
use Neos\ContentRepository\Domain\Model\NodeTemplate;
use Neos\ContentRepository\Domain\Model\Node;
use Neos\ContentRepository\Utility as CrUtitlity;
use Reomi\Football\Service\BetService;
use Reomi\Football\Service\ApiService;
use Neos\Error\Messages as Error;

class PredictController extends ActionController
{

    /**
     * @var BetService
     * @Flow\Inject
     */
    protected $betService;

//
//    /**
//     * Injects the Security Context
//     *
//     * @param Context $securityContext
//     * @return void
//     */
//    public function injectSecurityContext(Context $securityContext)
//    {
//        $this->securityContext = $securityContext;
//    }

    /**
     *
     */
    public function scoreAction()
    {
        $arguments = $this->request->getArguments();

        $data = unserialize(base64_decode($arguments['serialized']));

        $unserialized = unserialize(base64_decode($this->request->getInternalArgument('__referrer')['arguments']));

//        $matchStatus = new ApiService();
//        $matchStatus->getMatch($data['data']['self']);
//        $response = $matchStatus->getResponse();
//
//        $responseData = json_decode($response, true);

        $now = new \DateTime();
        $matchDate = new \DateTime($data['date']);
        $diff = ($matchDate->getTimestamp() + (60*5)) - $now->getTimestamp();

        if( $diff <= 0){
            $this->addFlashMessage('Time for bet has ended.','',  'Error');
            $this->redirect('show', 'Frontend\Node','Neos.Neos', $unserialized);
        }

        $data['data']['matchAllData'] = $arguments['serialized'];
        $data['data']['status'] = 'OPEN';
        $data['data']['title'] = $data['homeTeamName'].' - '.$data['awayTeamName'].'|'.$matchDate->format('Y-m-d H:i');

        $newBet = $this->betService->createBet($data['data']['self'], array(
           'homePredict' => $arguments['homePredict'],
           'awayPredict' => $arguments['awayPredict']
        ), $data['data']);


        $this->addFlashMessage('Your bet has been saved. You can see history of you bets on Profile page');
        $this->redirect('show', 'Frontend\Node','Neos.Neos', $unserialized);

    }


//    /**
//     *
//     */
//    public function indexAction()
//    {
//        die('index');
//    }
//
//    /**
//     *
//     */
//    public function readAction()
//    {
//        die('read');
//    }
//
//    /**
//     *
//     */
//    public function writeAction()
//    {
//        die('write');
//    }

}
