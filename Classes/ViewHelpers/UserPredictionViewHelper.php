<?php
namespace Reomi\Football\ViewHelpers;

use Neos\Flow\Configuration\ConfigurationManager;
use Neos\Flow\Exception;
use Neos\Flow\Security\Context;
use Neos\FluidAdaptor\Core\ViewHelper\AbstractViewHelper;
use Neos\ContentRepository\Domain\Service\ContextFactoryInterface;

class UserPredictionViewHelper extends AbstractViewHelper{

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @param string $path
     * @return string
     * @throws \Exception
     */
    public function render($fixture = null)
    {
        if(!$fixture)
            throw new Exception('Fixture argument is requiered');

        $objectManager = $this->renderingContext->getObjectManager();

        $securityContext = $objectManager->get(Context::class);

        if( !$securityContext )
            return FALSE;

        $accountIdentifier = $securityContext->getAccount()->getAccountIdentifier();

        if( !$accountIdentifier ){
            return FALSE;
        }

        $contextFactory = $objectManager->get(ContextFactoryInterface::class);

        $context = $contextFactory->create(array(
            'workspaceName' => 'live'
        ));

        $configurationManager = $objectManager->get(ConfigurationManager::class);

        $settings = $configurationManager->getConfiguration('Settings');

        $rootPath = $settings['Reomi']['Football']['contentRepository']['rootNodeName'];

        $rootPath .= '/'.$fixture.'/bets/'.$accountIdentifier;

        $node = $context->getRootNode()->getNode('sites/'.$rootPath);

        # get the template variable container
        $templateVariableContainer = $this->renderingContext->getTemplateVariableContainer();
        # add a variable to the context
        $templateVariableContainer->add('bet', $node);
        # render the children, the variable salutation is available for the child view helpers
        $result = $this->renderChildren();
        # remove the added variable again from the context
        $templateVariableContainer->remove('bet');
        return $result;
    }

}